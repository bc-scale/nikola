# Test the demo site to ensure the image can build it
# Copyright (C) 2022 Victor Santos
#
# License: GPL-3.0+

import sys
import urllib3

BASE_URL=sys.argv[1]

def rel_path(path = ''):
    res = BASE_URL
    if path != '':
        res = '{0}/{1}'.format(BASE_URL, path)
    return res

def wget(pool_manager, url = '', method = 'GET'):
    return pool_manager.request(method, url)

http = urllib3.PoolManager()

urls = [
    '',
    'archive.html',
    'categories/index.html',
    'rss.xml',
    'posts/welcome-to-nikola/',
    'pages/handbook/',
    'galleries/demo/',
    'pages/listings-demo/',
    'pages/dr-nikolas-vendetta/'
]
for rpath in urls:
    url = rel_path(rpath)
    print("Testing \"{0}\"...".format(url), end = " ")
    r = wget(http, url)
    if r.status != 200:
        raise HTTPError("\"{0}\": Not found".format(rpath))
    print("Status: {0}".format(r.status))
