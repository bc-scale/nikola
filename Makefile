.PHONY: test

all: build test

build: Dockerfile
	@docker build -t registry.gitlab.com/padawanphysicist/nikola .

test:
	@echo "Testing image..."
	@docker run --rm --volume $$PWD:/root --workdir /root --user $$(id -u):$$(id -g) registry.gitlab.com/padawanphysicist/nikola bash test.sh
	@rm -rf test/
