#  Nikola run-time environment
#  Copyright (C) 2022 Victor Santos
#
#  License: GPL-3.0+

FROM python:3.9.13-slim
LABEL author.name="Victor Santos" \
      author.email="vct.santos@protonmail.com"

# Disable pip caching and version checking
ENV PIP_OPTS --no-input --no-cache-dir --disable-pip-version-check --upgrade
# Avoid to write .pyc files on the import of source modules
ENV PYTHONDONTWRITEBYTECODE 1
# Ensures that the python output i.e. the stdout and stderr streams are sent
# straight to terminal
ENV PYTHONUNBUFFERED 1

ARG _VERSION

COPY requirements.txt /tmp/requirements.txt
COPY constraints.txt /tmp/constraints.txt
RUN apt-get update -qq && \
    apt-get install -y curl && \
    curl --remote-name --location https://github.com/sass/dart-sass/releases/download/1.54.3/dart-sass-1.54.3-linux-x64.tar.gz && \
    tar zxvf dart-sass-1.54.3-linux-x64.tar.gz && \
    mv dart-sass/sass /usr/bin/sass && \
    rm -rf dart-sass && \
    apt-get remove --purge -y curl && \
    apt autoremove -y && \
    pip install $PIP_OPTS pip setuptools wheel && \
    pip install $PIP_OPTS --requirement /tmp/requirements.txt \
                          --constraint  /tmp/constraints.txt  \
    "Nikola[extras]" && \
    rm -rf /tmp/*

